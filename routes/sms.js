const express = require('express');
const Joi = require('joi');

const database = require('../models/database');
const config = require('../config')();
const { basicAuth } = require('../services/Auth');
const smsValidation = require('../validations/sms');

const { registerStopSMS, isNotAllowedToSendSMS } = require('../services/StopSMSRequest');
const { checkOuboundSMSRateLimit, incrementCount } = require('../services/SMSRateLimit');

var router = express.Router();

/* Inbound SMS webhook */
router.post('/inbound', basicAuth, smsValidation, async (req, res, next) => {
  const { from, to, text } = req.body;
  if (config.stopTextPattern.test(text)) {
    await registerStopSMS(from, to);
  }
  res.json({message: 'inbound sms is ok', error: ''});
}).all('/inbound', (req, res) => res.status(405).send());

/* Outbound API */
router.post('/outbound', basicAuth, smsValidation, async (req, res, next) => {
  const { from, to, text } = req.body;
  const { exceeded, timetoRetry } = await checkOuboundSMSRateLimit(from);
  if (exceeded) {
    res.status(429);
    res.set('Retry-After', timetoRetry);
    res.json({message: '', error: `limit reached for from: ${from}`});
    return;
  }

  const isNotAllowedToSend = await isNotAllowedToSendSMS(from, to);
  if (isNotAllowedToSend) {
    res.status(403);
    res.json({message: '', error: `sms from ${from} to ${to} is blocked by STOP request`});
    return;
  }

  await incrementCount(from);
  res.json({message: 'outbound sms is ok', error: ''});
}).all('/outbound', (req, res) => res.status(405).send());

module.exports = router;
