const chai = require('chai');
const chaiHttp = require('chai-http');

// Reducing the time and no of SMS for faster testing
const config = require('../config')();
const app = require('../app');
const redisClient = require('../services/RedisFactory');
const apikeysAndSecrets = require('../services/sample_api_key_secrets');

const expect = chai.expect;

chai.use(chaiHttp);

const successMessageOutbound = 'outbound sms is ok';
const validExampleText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
const fromNumber = '329230931212';
const toNumber = '3232327723';
const fromNumber2 = '32923093';
const fromNumber3 = '32923093323';
const fromNumber4 = '3292323093323';
const limitReachedMessage = `limit reached for from: ${fromNumber}`;
const limitReachedMessage2 = `limit reached for from: ${fromNumber2}`;
const limitReachedMessage3 = `limit reached for from: ${fromNumber3}`;
const limitReachedMessage4 = `limit reached for from: ${fromNumber4}`;

const firstKey = Object.keys(apikeysAndSecrets)[0];
const authToken = Buffer.from(`${firstKey}:${apikeysAndSecrets[firstKey]}`).toString('base64');

describe('Rate limit outbound sms - Integration test', function() {
  it('Should return limit reached message when sms rate limit is reached', function(done) {
    this.timeout(4000);
    let numberOfSMSSent = 0;
    function sendOutBoundSMS(callback) {
      chai.request(app)
      .post('/sms/outbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: fromNumber, to: toNumber, text: validExampleText})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageOutbound);
        numberOfSMSSent += 1;
        if (numberOfSMSSent <= config.outboundSMSLimit) {
          sendOutBoundSMS(() => {
            callback();
          });
        } else {
          callback();
        }
      });
    }

    redisClient.del(`${fromNumber}${config.redisRateLimitKeySuffix}`, function() {
      sendOutBoundSMS(() => {
        chai.request(app)
        .post('/sms/outbound')
        .set('content-type', 'application/json')
        .set('Authorization', `Basic ${authToken}`)
        .send({from: fromNumber, to: toNumber, text: validExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(429);
          expect(res).to.have.header('Retry-After');
          expect(JSON.parse(res.error.text).error).to.equal(limitReachedMessage);
          done();
        });
      });
    });
  });

  it('Should return limit reached message when sms rate limit is reached even when cache is missing', function(done) {
    this.timeout(4000);
    let numberOfSMSSent = 0;
    function sendOutBoundSMS(callback) {
      chai.request(app)
      .post('/sms/outbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: fromNumber3, to: toNumber, text: validExampleText})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageOutbound);
        numberOfSMSSent += 1;
        if (numberOfSMSSent <= config.outboundSMSLimit) {
          sendOutBoundSMS(() => {
            callback();
          });
        } else {
          callback();
        }
      });
    }

    global.dbPool.connect()
    .then((dbClient) => {
      return dbClient.query('DELETE from outbound_sms_count WHERE from_number=$1', [fromNumber3]);
    })
    .then(() => {
      return redisClient.delAsync(`${fromNumber3}${config.redisRateLimitKeySuffix}`);
    })
    .then(() => {
      sendOutBoundSMS(() => {
        redisClient.del(`${fromNumber3}${config.redisRateLimitKeySuffix}`, function() {
          chai.request(app)
          .post('/sms/outbound')
          .set('content-type', 'application/json')
          .set('Authorization', `Basic ${authToken}`)
          .send({from: fromNumber3, to: toNumber, text: validExampleText})
          .end(function(err, res) {
            expect(res).to.have.status(429);
            expect(res).to.have.header('Retry-After');
            expect(JSON.parse(res.error.text).error).to.equal(limitReachedMessage3);
            done();
          });
        });
      });
    });
  });

  it('Should be able to send message after the rateLimitSeconds', function(done) {
    let numberOfSMSSent = 0;
    this.timeout(6000);
    function sendOutBoundSMS(callback) {
      chai.request(app)
      .post('/sms/outbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: fromNumber2, to: toNumber, text: validExampleText})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageOutbound);
        numberOfSMSSent += 1;
        if (numberOfSMSSent <= config.outboundSMSLimit) {
          sendOutBoundSMS(() => {
            callback();
          });
        } else {
          callback();
        }
      });
    }

    redisClient.del(`${fromNumber2}${config.redisRateLimitKeySuffix}`, function() {
      sendOutBoundSMS(() => {
        chai.request(app)
        .post('/sms/outbound')
        .set('content-type', 'application/json')
        .set('Authorization', `Basic ${authToken}`)
        .send({from: fromNumber2, to: toNumber, text: validExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(429);
          expect(res).to.have.header('Retry-After');
          expect(JSON.parse(res.error.text).error).to.equal(limitReachedMessage2);

          setTimeout(() => {
            chai.request(app)
            .post('/sms/outbound')
            .set('content-type', 'application/json')
            .set('Authorization', `Basic ${authToken}`)
            .send({from: fromNumber2, to: toNumber, text: validExampleText})
            .end(function(err, res) {
              expect(res).to.have.status(200);
              expect(JSON.parse(res.text).message).to.equal(successMessageOutbound);
              done();
            });
          }, 4100);
        });
      });
    });
  });


  it('Should be able to send message after the rateLimitSeconds even whem the cache is missing', function(done) {
    let numberOfSMSSent = 0;
    this.timeout(6000);
    function sendOutBoundSMS(callback) {
      chai.request(app)
      .post('/sms/outbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: fromNumber4, to: toNumber, text: validExampleText})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageOutbound);
        numberOfSMSSent += 1;
        if (numberOfSMSSent <= config.outboundSMSLimit) {
          sendOutBoundSMS(() => {
            callback();
          });
        } else {
          callback();
        }
      });
    }

    global.dbPool.connect()
    .then((dbClient) => {
      return dbClient.query('DELETE from outbound_sms_count WHERE from_number=$1', [fromNumber4]);
    })
    .then(() => {
      return redisClient.delAsync(`${fromNumber4}${config.redisRateLimitKeySuffix}`);
    })
    .then(() => {
      sendOutBoundSMS(() => {
        redisClient.del(`${fromNumber4}${config.redisRateLimitKeySuffix}`, function() {
          chai.request(app)
          .post('/sms/outbound')
          .set('content-type', 'application/json')
          .set('Authorization', `Basic ${authToken}`)
          .send({from: fromNumber4, to: toNumber, text: validExampleText})
          .end(function(err, res) {
            expect(res).to.have.status(429);
            expect(res).to.have.header('Retry-After');
            expect(JSON.parse(res.error.text).error).to.equal(limitReachedMessage4);

            setTimeout(() => {
              chai.request(app)
              .post('/sms/outbound')
              .set('content-type', 'application/json')
              .set('Authorization', `Basic ${authToken}`)
              .send({from: fromNumber4, to: toNumber, text: validExampleText})
              .end(function(err, res) {
                expect(res).to.have.status(200);
                expect(JSON.parse(res.text).message).to.equal(successMessageOutbound);
                done();
              });
            }, 4100);
          });
        });
      });
    });
  });
});
