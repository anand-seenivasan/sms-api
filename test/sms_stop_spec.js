const chai = require('chai');
const chaiHttp = require('chai-http');

const app = require('../app');
const redisClient = require('../services/RedisFactory');
const apikeysAndSecrets = require('../services/sample_api_key_secrets');

const expect = chai.expect;
chai.use(chaiHttp);

const numberA = 82392238312345;
const numberB = 323283833;

const numberC = 8239283432345;
const numberD = 323243833;

const successMessageInbound = 'inbound sms is ok';
const successMessageOutbound = 'outbound sms is ok';
const validExampleText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
const nonStopText = 'Please STOPakdsf sending me sms';
const stopText = 'Please STOP sending me sms';
const stopText2 = 'Please stop sending me sms';
const stopText3 = 'Please sTop\r sending me sms';
const stopText4 = 'Please stop\r\n sending me sms';
const stopText5 = 'Please STOP\n sending me sms';
const blockedResponse = `sms from ${numberD} to ${numberC} is blocked by STOP request`;

const firstKey = Object.keys(apikeysAndSecrets)[0];
const authToken = Buffer.from(`${firstKey}:${apikeysAndSecrets[firstKey]}`).toString('base64');

describe('STOP request', function() {
  it('Should not block outbound sms if STOP request is not sent', function(done) {
    chai.request(app)
    .post('/sms/inbound')
    .set('content-type', 'application/json')
    .set('Authorization', `Basic ${authToken}`)
    .send({from: numberA, to: numberB, text: validExampleText})
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(JSON.parse(res.text).message).to.equal(successMessageInbound);

      chai.request(app)
      .post('/sms/outbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: numberB, to: numberA, text: validExampleText})

      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageOutbound);
        done();
      });
    });
  });

  it('Should not block outbound sms when for incorrect stop words', function(done) {
    chai.request(app)
    .post('/sms/inbound')
    .set('content-type', 'application/json')
    .set('Authorization', `Basic ${authToken}`)
    .send({from: numberA, to: numberB, text: nonStopText})
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(JSON.parse(res.text).message).to.equal(successMessageInbound);

      chai.request(app)
      .post('/sms/outbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: numberB, to: numberA, text: validExampleText})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageOutbound);
        done();
      });
    });
  });

  it('Should block outbound sms if STOP request is registered by the "to" number',  function(done) {
    redisClient.del(`${numberD}_stop`, function() {
      chai.request(app)
      .post('/sms/inbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: numberC, to: numberD, text: stopText})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageInbound);

        chai.request(app)
        .post('/sms/outbound')
        .set('content-type', 'application/json')
        .set('Authorization', `Basic ${authToken}`)
        .send({from: numberD, to: numberC, text: validExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(403);
          expect(JSON.parse(res.error.text).error).to.equal(blockedResponse);
          done();
        });
      });
    });
  });


  it('Should block outbound sms if "stop" request is registered by the "to" number',  function(done) {
    redisClient.del(`${numberD}_stop`, function() {
      chai.request(app)
      .post('/sms/inbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: numberC, to: numberD, text: stopText2})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageInbound);

        chai.request(app)
        .post('/sms/outbound')
        .set('content-type', 'application/json')
        .set('Authorization', `Basic ${authToken}`)
        .send({from: numberD, to: numberC, text: validExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(403);
          expect(JSON.parse(res.error.text).error).to.equal(blockedResponse);
          done();
        });
      });
    });
  });


  it('Should block outbound sms if STOP\\r request is registered by the "to" number',  function(done) {
    redisClient.del(`${numberD}_stop`, function() {
      chai.request(app)
      .post('/sms/inbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: numberC, to: numberD, text: stopText3})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageInbound);

        chai.request(app)
        .post('/sms/outbound')
        .set('content-type', 'application/json')
        .set('Authorization', `Basic ${authToken}`)
        .send({from: numberD, to: numberC, text: validExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(403);
          expect(JSON.parse(res.error.text).error).to.equal(blockedResponse);
          done();
        });
      });
    });
  });

  it('Should block outbound sms if STOP\\r\\n request is registered by the "to" number',  function(done) {
    redisClient.del(`${numberD}_stop`, function() {
      chai.request(app)
      .post('/sms/inbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: numberC, to: numberD, text: stopText4})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageInbound);

        chai.request(app)
        .post('/sms/outbound')
        .set('content-type', 'application/json')
        .set('Authorization', `Basic ${authToken}`)
        .send({from: numberD, to: numberC, text: validExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(403);
          expect(JSON.parse(res.error.text).error).to.equal(blockedResponse);
          done();
        });
      });
    });
  });

  it('Should block outbound sms if STOP\\n request is registered by the "to" number',  function(done) {
    redisClient.del(`${numberD}_stop`, function() {
      chai.request(app)
      .post('/sms/inbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: numberC, to: numberD, text: stopText5})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageInbound);

        chai.request(app)
        .post('/sms/outbound')
        .set('content-type', 'application/json')
        .set('Authorization', `Basic ${authToken}`)
        .send({from: numberD, to: numberC, text: validExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(403);
          expect(JSON.parse(res.error.text).error).to.equal(blockedResponse);
          done();
        });
      });
    });
  });

  it('Should block outbound sms if STOP request is registered by the "to" number even when cache expires',  function(done) {
    this.timeout(4000);
    redisClient.del(`${numberD}_stop`, function() {
      chai.request(app)
      .post('/sms/inbound')
      .set('content-type', 'application/json')
      .set('Authorization', `Basic ${authToken}`)
      .send({from: numberC, to: numberD, text: stopText5})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(JSON.parse(res.text).message).to.equal(successMessageInbound);

        setTimeout(() => {
          chai.request(app)
          .post('/sms/outbound')
          .set('content-type', 'application/json')
          .set('Authorization', `Basic ${authToken}`)
          .send({from: numberD, to: numberC, text: validExampleText})
          .end(function(err, res) {
            expect(res).to.have.status(403);
            expect(JSON.parse(res.error.text).error).to.equal(blockedResponse);
            done();
          });
        }, 2500);
      });
    });
  });
});
