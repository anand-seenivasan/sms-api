const chai = require('chai');
const chaiHttp = require('chai-http');

const app = require('../app');
const apikeysAndSecrets = require('../services/sample_api_key_secrets');

const expect = chai.expect;

chai.use(chaiHttp);

const toParamterMissingMsg = 'to is missing';
const fromParamterMissingMsg = 'from is missing';
const textParamterMissingMsg = 'text is missing';
const toParamterInvalidMsg = 'to is invalid';
const fromParamterInvalidMsg = 'from is invalid';
const textParamterInvalidMsg = 'text is invalid';
const successMessage = 'inbound sms is ok';

const invalidExampleText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.";
const validExampleText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";

const firstKey = Object.keys(apikeysAndSecrets)[0];
const authToken = Buffer.from(`${firstKey}:${apikeysAndSecrets[firstKey]}`).toString('base64');
const wrongAuthToken = Buffer.from(`wronguser:pass`).toString('base64');

describe('/sms/inbound', function() {
  describe('Appropriate status code', function() {
    it('Should return 405 status if GET method is used', function(done) {
      chai.request(app)
      .get('/sms/inbound')
      .end(function(err, res) {
        expect(res).to.have.status(405);
        done();
      });
    });

    it('Should return 405 status if PUT method is used', function(done) {
      chai.request(app)
      .put('/sms/inbound')
      .end(function(err, res) {
        expect(res).to.have.status(405);
        done();
      });
    });

    it('Should return 405 status if DELETE method is used', function(done) {
      chai.request(app)
      .delete('/sms/inbound')
      .end(function(err, res) {
        expect(res).to.have.status(405);
        done();
      });
    });
  });

  describe('Paramter validations', function () {
    describe('from paramter validations', function () {
      it('Should return missing "from" parameter response if "from" is not in the request', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(fromParamterMissingMsg);
          done();
        });
      });

      it('Should return invalid "from" parameter response if "from" string paramter length is < 6', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: '2323'})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(fromParamterInvalidMsg);
          done();
        });
      });

      it('Should return invalid "from" parameter response if "from" string paramter length is > 16', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: '232382382389239832323'})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(fromParamterInvalidMsg);
          done();
        });
      });

      it('Should return invalid "from" parameter response if "from" paramter is alphanumberic', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: 'ksdjf223'})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(fromParamterInvalidMsg);
          done();
        });
      });

      it('Should return invalid "from" parameter response if "from" integer paramter length is < 6', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: 2323})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(fromParamterInvalidMsg);
          done();
        });
      });

      it('Should return invalid "from" parameter response if "from" integer paramter length is > 16', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: 232382398238923232338})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(fromParamterInvalidMsg);
          done();
        });
      });
    });

    describe('to paramter validations', function () {
      it('Should return missing "to" parameter response if "to" is not in the request', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: '82392830923'})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(toParamterMissingMsg);
          done();
        });
      });

      it('Should return invalid "to" parameter response if "to" string paramter length is < 6', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({ from: '82392830923', to: '2323'})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(toParamterInvalidMsg);
          done();
        });
      });

      it('Should return invalid "to" parameter response if "to" string paramter length is > 16', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: '232382398', to: '239803902930912333'})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(toParamterInvalidMsg);
          done();
        });
      });

      it('Should return invalid "to" parameter response if "to" paramter is alphanumberic', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: '82392830923', to: 'ksdjf2323'})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(toParamterInvalidMsg);
          done();
        });
      });

      it('Should return invalid "to" parameter response if "to" integer paramter length is < 6', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: 232323232332, to: 2339})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(toParamterInvalidMsg);
          done();
        });
      });

      it('Should return invalid "to" parameter response if "to" integer paramter length is > 16', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: 232382332, to: 239803902930912333})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(toParamterInvalidMsg);
          done();
        });
      });
    });
    describe('text paramter validations', function () {
      it('Should return missing "text" parameter response if "to" paramter length is missing', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: '82392830923', to: '323232323'})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(textParamterMissingMsg);
          done();
        });
      });

      it('Should return invalid "text" parameter response if "text" paramter length is 0', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: '82392830923', to: '323232323', text: ''})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(textParamterInvalidMsg);
          done();
        });
      });

      it('Should return invalid "text" parameter response if "text" paramter length is > 120', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: '82392830923', to: '323232323', text: invalidExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(500);
          expect(JSON.parse(res.error.text).error).to.equal(textParamterInvalidMsg);
          done();
        });
      });
    });
    describe('Successful response', function () {
      it('Should return invalid "text" parameter response if "text" paramter length is > 120', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${authToken}`)
        .set('content-type', 'application/json')
        .send({from: '82392830923', to: '323232323', text: validExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(200);
          expect(JSON.parse(res.text).message).to.equal(successMessage);
          done();
        });
      });
    });

    describe('Basic Auth', function () {
      it('Should return Unauthoried response when not Authorization header present', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('content-type', 'application/json')
        .send({from: '823920923', to: '323232323', text: validExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(401);
          expect(JSON.parse(res.error.text).error).to.equal('Unauthoried');
          done();
        });
      });

      it('Should return Unauthoried response for wrong credentials', function(done) {
        chai.request(app)
        .post('/sms/inbound')
        .set('Authorization', `Basic ${wrongAuthToken}`)
        .set('content-type', 'application/json')
        .send({from: '823920923', to: '323232323', text: validExampleText})
        .end(function(err, res) {
          expect(res).to.have.status(401);
          expect(JSON.parse(res.error.text).error).to.equal('Unauthoried');
          done();
        });
      });
    });
  });
});
