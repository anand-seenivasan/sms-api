const { Pool } = require('pg')
const os = require('os');

let connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/postgres';

const pool = new Pool({ connectionString });
global.dbPool = pool;

async function init() {
  const client = await pool.connect()
  try {
      await client.query(`CREATE TABLE if not exists stop_outbound_sms(
        id SERIAL PRIMARY KEY,
        from_number VARCHAR(16) not null,
        to_number VARCHAR(16) not null,
        UNIQUE (from_number, to_number)
      )`);
      await client.query(`CREATE TABLE if not exists outbound_sms_count(
        from_number VARCHAR(16) PRIMARY KEY,
        count integer not null,
        created_time timestamp not null
      )`);
  } catch (err) {
    console.log(err)
  } finally {
    client.release()
  }
}
init();
