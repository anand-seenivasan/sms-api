let config = {
  redisStopKeySuffix: '_stop',
  redisRateLimitKeySuffix: '_outbound_count',
  rateLimitSeconds: 60 * 60,
  outboundSMSLimit: 50,
  durationToCacheStop: 4 * 60 * 60,
  stopTextPattern: /\bSTOP\b/i
};

// overide is for unit testing.
module.exports = function(overide) {
  config = { ...config, ...overide };
  return config;
};
