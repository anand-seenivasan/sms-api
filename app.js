var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var smsRouter = require('./routes/sms');

var app = express();
app.set('view engine', 'jade');

if (process.env.NODE_ENV !== 'test') {
  app.use(logger('dev'));
}

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => res.send('Welcome!'));
app.use('/sms', smsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  let response = req.app.get('env') === 'dev' ? { message: err.message, stack: err.stack } : {
    message: '', error: 'unknown failure'
  };
  console.log(err);
  if (err.type === 'entity.parse.failed') {
    response = { message: "", error: 'invalid input' };
  }

  if (err.status === 404) {
    response = { message: "", error: 'Not Found' };
  }

  // render the error page
  res.status(err.status || 500);
  res.send(response);
});

module.exports = app;
