const Joi = require('joi');

const smsSchema = Joi.object({
  from: Joi.alternatives().try(Joi.string().regex(/^\d+$/).min(6).max(16), Joi.number().min(100000).max(9999999999999999)).required(),
  to: Joi.alternatives().try(Joi.string().regex(/^\d+$/).min(6).max(16), Joi.number().min(100000).max(9999999999999999)).required(),
  text: Joi.string().min(1).max(120).required(),
});

module.exports = (req, res, next) => {
  const validationResult = Joi.validate(req.body, smsSchema);
  if (validationResult.error) {
    const response = { message: '' };
    const paramter = validationResult.error.details[0].path[0];
    if (validationResult.error.details[0].type === 'any.required') {
      response.error = `${paramter} is missing`;
    } else {
      response.error = `${paramter} is invalid`;
    }
    res.status(500);
    res.json(response);
    return;
  }
  next();
};
