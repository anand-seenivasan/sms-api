## Required softwares
1) [Node.js 8.x.x](https://nodejs.org/en/download/)

2) [redis](https://redis.io/topics/quickstart)

3) [PostgreSQL](https://wiki.postgresql.org/wiki/Detailed_installation_guides)

## Prerequisites to run dev server and to run test cases

Run redis and postgresql locally.

## Installing dependencies
```npm install```

## To start test
```npm test```

## To start Dev Environment
```npm run dev```

Runs in ```localhost:3000```

## Production Environment variables
```REDIS_PORT, REDIS_HOST, REDIS_PASSWORD, DATABASE_URL```

```DATABASE_URL``` - Postgres connection URL

## Request sample
Urls:

Inbound - ```/sms/inbound```

Outbound - ```/sms/outbound```

Header:
```js
{
  "Content-Type": "application/json",
  "Authorization": "Basic ZTAzb3BhMkJWU0E2Q0dwVEdXS3p3czNRVUwzVUd4VWU6SmtSMGFrdzgwM0M5d29sQ1VzU0RDNFFrS1NJbGpwMUc="
}
```

Paramters:

```js
{
  "from": "12132123123"/ 12132123123,
  "to": "12132123123"/ 12132123123,
  "text": "Hello World"
}
```
