const config = require('../config')();
const redisClient = require('./RedisFactory');

const checkOuboundSMSRateLimit = async (from) => {
  const defaultResponse = { exceeded: false, timetoRetry: 0 };
  const cacheExist = await redisClient.existsAsync(`${from}${config.redisRateLimitKeySuffix}`);
  if (cacheExist) {
    const outboundCount = await redisClient.getAsync(`${from}${config.redisRateLimitKeySuffix}`);
    if (outboundCount > config.outboundSMSLimit) {
      const timeToLive = await redisClient.ttlAsync(`${from}${config.redisRateLimitKeySuffix}`);
      defaultResponse.exceeded = true;
      defaultResponse.timetoRetry = timeToLive;
    }
  } else {
    const dbClient = await global.dbPool.connect();
    try {
      const response = await dbClient.query('SELECT count, created_time from outbound_sms_count WHERE from_number=$1', [from]);
      if (response.rows.length > 0) {
        const createdTime = new Date(response.rows[0].created_time).getTime() / 1000;
        const difference = new Date().getTime() / 1000 - createdTime;
        const count = response.rows[0].created_time;
        if (difference < config.rateLimitSeconds && count > config.outboundSMSLimit) {
          defaultResponse.exceeded = true;
          defaultResponse.timetoRetry = Math.floor(config.rateLimitSeconds - difference);
        }
      }
    } finally {
      await dbClient.release();
    }
  }
  return defaultResponse;
}

const incrementCount = async (from) => {
  const timeToLive = await redisClient.ttlAsync(`${from}${config.redisRateLimitKeySuffix}`);
  let timeToExpire;
  if (timeToLive < 0) {
    timeToExpire = new Date().getTime()/1000 + config.rateLimitSeconds;
  } else {
    timeToExpire = new Date().getTime()/1000 + timeToLive;
  }
  await redisClient.incrAsync(`${from}${config.redisRateLimitKeySuffix}`);
  await redisClient.expireatAsync(`${from}${config.redisRateLimitKeySuffix}`, Math.floor(timeToExpire));
  const dbClient = await global.dbPool.connect();
  try {
    await dbClient.query(`INSERT INTO outbound_sms_count (from_number, count, created_time) VALUES ($1, $2, $3)
      ON CONFLICT (from_number) DO UPDATE SET count = excluded.count + 1`, [from, 1, new Date()]);
  } finally {
    await dbClient.release();
  }
};

module.exports = {
  checkOuboundSMSRateLimit,
  incrementCount,
};
