const config = require('../config')();
const redisClient = require('./RedisFactory');

const registerStopSMS = async (from, to) => {
  await redisClient.saddAsync(`${to}${config.redisStopKeySuffix}`, from);
  await redisClient.expireAsync(`${to}${config.redisStopKeySuffix}`, config.durationToCacheStop);
  const dbClient = await global.dbPool.connect();
  try {
    await dbClient.query(`INSERT INTO stop_outbound_sms (from_number, to_number) values($1, $2)
      ON CONFLICT DO NOTHING`, [to, from]);
  } finally {
    await dbClient.release();
  }
}

const isNotAllowedToSendSMS = async (from, to) => {
  let isNotAllowedToSend = false;
  const cacheExist = await redisClient.existsAsync(`${from}${config.redisStopKeySuffix}`);
  if (!cacheExist) {
    const dbClient = await global.dbPool.connect();
    try {
      const response = await dbClient.query('SELECT from_number from stop_outbound_sms where from_number=$1 and to_number=$2', [from, to]);
      isNotAllowedToSend = response.rows.length > 0;
    } finally {
      dbClient.release();
    }
  } else {
    isNotAllowedToSend = await redisClient.sismemberAsync(`${from}${config.redisStopKeySuffix}`, to);
  }
  return isNotAllowedToSend;
};

module.exports = {
  registerStopSMS,
  isNotAllowedToSendSMS
};
