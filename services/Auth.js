const auth = require('basic-auth');
const apikeysAndSecrets = require('./sample_api_key_secrets');

const basicAuth = (req, res, next) => {
  var user = auth(req);
  if (!user || apikeysAndSecrets[user.name] !== user.pass) {
    res.status(401);
    res.json({message: '', error: 'Unauthoried'});
    return;
  }
  next();
}

module.exports = {
  basicAuth
}
