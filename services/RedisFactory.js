const redis = require('redis');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis);
let redisClient;

if (process.env.NODE_ENV != 'dev' && process.env.NODE_ENV != 'test') {
  redisClient = redis.createClient(process.env.REDIS_PORT , process.env.REDIS_HOST, {no_ready_check: true});
  redisClient.auth(process.env.REDIS_PASSWORD, function (err) {
      if (err) throw err;
  });
  redisClient.on('connect', function() {
      console.log('Connected to Redis');
  });
} else {
  redisClient = redis.createClient();
}


module.exports = redisClient;
